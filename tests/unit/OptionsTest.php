<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Contracts\Optionable;
use Symbiont\Config\Tests\OptionClass;

final class OptionsTest extends TestCase {

    protected ?Optionable $object = null;
    protected ?array $options = null;

    protected function setUp(): void {
        $this->options = [
            'first' => true, // option
            'second' => false, // option
            1,
            2,
            'some-value', // flag
            'another-value', // flag
            'dont' => [ // option
                'like' => 'values'
            ]
        ];
        $this->object = new OptionClass($this->options);
    }

    public function testHasOption() {
        $this->assertTrue($this->object->hasOption('first'));
        $this->assertTrue($this->object->hasOption('second'));
        $this->assertFalse($this->object->hasFlag('first'));
        $this->assertFalse($this->object->hasFlag('second'));
        $this->assertTrue($this->object->hasOption('dont'));
    }

    public function testHasFlag() {
        $this->assertTrue($this->object->hasFlag('some-value'));
        $this->assertTrue($this->object->hasFlag('another-value'));
        $this->assertFalse($this->object->hasFlag('dont'));
        $this->assertFalse($this->object->hasFlag('doesnotexist'));
    }

    public function testOption() {
        $this->assertTrue($this->object->option('first'));
        $this->assertFalse($this->object->option('second'));
        $this->assertEquals('default-value', $this->object->option('third', 'default-value'));
        $this->assertNull($this->object->option('some-value'));
        $this->assertNull($this->object->option('doesnotexist'));
    }

    public function testFlag() {
        $this->assertTrue($this->object->flag('some-value'));
        // should take default value
        $this->assertTrue($this->object->flag('does-not-exist', true));
        // none existing flags should return false by default
        $this->assertFalse($this->object->flag('does-not-exist'));

        $this->assertTrue($this->object->flag(1));
        $this->assertTrue($this->object->flag(2));
        // should take default value
        $this->assertTrue($this->object->flag(3, true));
        // none existing flags should return false by default
        $this->assertFalse($this->object->flag(3));
    }

    public function testGetOptionsFlags() {
        $options = [1, 2, 'option1' => true, 'flag1', 'flag2', 'option2' => false];
        $object = new OptionClass($options);
        $this->assertEquals($options, $object->getOptions());
        $this->assertEquals([1, 2, 'flag1', 'flag2'], $object->getFlags());
    }

    public function testRemoveOption() {
        $expected = $this->options;
        unset($expected['second']);
        $this->object->removeOption('second');
        $actual =  $this->object->getOptions();
        $this->assertEquals($expected, $actual);

        // remove when retrieved
        unset($expected['first']);
        $this->assertTrue($this->object->option('first', 'default-value', true));
        $this->assertEquals($expected, $this->object->getOptions());


    }
}