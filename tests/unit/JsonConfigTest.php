<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Config;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\FileToStoreExistsException;
use Symbiont\Config\Exceptions\JsonDecodeErrorException;

final class JsonConfigTest extends TestCase {

    protected Configurable $config;
    protected string $file = './tests/tmp/.test-config.json';
    protected string $file_tmp = './tests/tmp/.tmp-config.json';
    protected string $file_invalid = './tests/tmp/.invalid-config.json';
    protected string $file_empty = './tests/tmp/.empty-config.json';

    public function __construct(string $name) {
        parent::__construct($name);

        if(file_exists($this->file_tmp)) {
            unlink($this->file_tmp);
        }
    }

    protected function setUp(): void {
        $this->config = Config::from($this->file);
    }

    protected function tearDown(): void {
        if(file_exists($this->file_tmp)) {
            unlink($this->file_tmp);
        }
    }

    protected function getValuesFromFile(): array {
        $this->assertFileExists($this->file);
        return json_decode(file_get_contents($this->file), true);
    }

    public function testValues() {
        $values = $this->getValuesFromFile();

        $this->assertEquals($values, $this->config->values());
    }

    public function testSetValues() {
        $values = [
            'some' => 'values',
            'being' => 'set'
        ];

        $this->config->values($values);
        $this->assertEquals($values, $this->config->values());
    }

    public function testGet() {
        $this->assertEquals($this->getValuesFromFile(), $this->config->get());
        $this->assertEquals("tested", $this->config->get('test'));
    }

    public function testGetDefault() {
        $default = 'default-value';
        $this->assertEquals($default, $this->config->get('key-does-not-exist', $default));
    }

    public function testSet() {
        $key = 'some';
        $value = 'value';

        $this->assertInstanceOf(Configurable::class, $this->config->set($key, $value));

        $values = $this->getValuesFromFile();
        $values[$key] = $value;
        $this->assertEquals($values, $this->config->values());
    }


    public function testLoad() {
        $this->assertInstanceOf(Configurable::class, $config = Config::from($this->file));
        $this->assertEquals($this->getValuesFromFile(), $config->values());
    }

    public function testLoadInvalidJson() {
        $this->expectException(JsonDecodeErrorException::class);
        Config::from($this->file_invalid);
    }

    public function testLoadEmptyJson() {
        $config = Config::from($this->file_empty);
        $this->assertEquals([], $config->values());
    }

    public function testSave() {
        $this->config->set('testing', 'save');
        $this->assertTrue($this->config->save());
        $this->assertInstanceOf(Configurable::class, $config = Config::from($this->file));
        $this->assertEquals('save', $config->get('testing'));
    }

    public function testStore() {
        $this->config->store($this->file_tmp);
        $this->assertFileExists($this->file_tmp);

        $this->expectException(FileToStoreExistsException::class);
        $this->config->store($this->file_tmp);
    }

    public function testToJson() {
        $values = [
            'this-will' => [
                "be tested" => "to json"
            ]
        ];
        $expected = '{"this-will":{"be tested":"to json"}}';
        $this->config->values($values);
        $this->assertEquals($expected, $this->config->toJson(flags: 0));
    }

    public function testFromJson() {
        $values = '{"this-will":{"be tested":"to json"}}';
        $expected = [
            'this-will' => [
                "be tested" => "to json"
            ]
        ];
        $this->config->fromJson($values);
        $this->assertEquals($expected, $this->config->values());
    }

}