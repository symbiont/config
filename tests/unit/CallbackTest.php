<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\ArrayConfig;
use Symbiont\Config\Drivers\FileDriver;

final class CallbackTest extends TestCase {

    protected array $events;

    protected function setUp(): void {
        $this->events = [
            FileDriver::CLB_LOADING,
            FileDriver::CLB_LOADED,
            FileDriver::CLB_SAVING,
            FileDriver::CLB_SAVED,
            FileDriver::CLB_STORING,
            FileDriver::CLB_STORED,
            FileDriver::CLB_UNLINKING,
            FileDriver::CLB_UNLINKED
        ];
    }

    public function testCallbackConstructorOptions() {
        $called = [];
        $callbacks = [];

        foreach($this->events as $event) {
            $callbacks[$event] = function() use ($event, &$called) {
                $called[] = $event;
            };
        }

        $config = new ArrayConfig('./tests/tmp/.test-config.php', [
            'callbacks' => $callbacks
        ]);

        $file_tmp = './tests/tmp/.tobedeleted.php';
        $config->save();
        if($config->store($file_tmp)) {
            $config->unlink();
        }

        foreach($this->events as $event) {
            $this->assertTrue(in_array($event, $called));
        }
    }

}