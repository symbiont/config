![release](https://gitlab.com/symbiont/config/-/badges/release.svg?key_text=version) ![pipeline](https://gitlab.com/symbiont/config/badges/master/pipeline.svg?key_text=test)

---

# Config

!!! warning
    This package is `work in progress`!

!!! Note
    This documentation only documents the technical usage of this package with little to no text.

Simple config library.

## Installation

Requires at least PHP `8.2`

```bash
composer require symbiont/config
```

---

## Testing

```php
composer test
```

## License

Released under MIT License
