# Usage

Minimal config library with support for `Array` and `JSON` files.

## Creation

Configs can be created specifically using `new Config`, `new ArrayConfig` or `new JsonConfig`. Configs can also 
automatically detect the file driver when using `Config::from($file)`.

```php
<?php
use Symbiont\Config\{Config, ArrayConfig, JsonConfig};

$config = new Config($options = []); // driverless by default
$config = new ArrayConfig($file, $options = []);
$config = new JsonConfig($file, $options = []);

$config = Config::from($file, $options = []);
// returns a driver based config.
```

!!! Note
    Only `.php` and `.json` supported for now.

## Default config methods

By default, `Symbiont\Config\Config` is not configured with any driver. It can be used as a base
class to implement any given config. Therefor it does not `load`, `save` or `store` any files.

| Method                                         |                              | Type          |
|:-----------------------------------------------|------------------------------|---------------|
| [`get(string $key)`](#configget)               | Get a value by key           | mixed         |
| [`set(string $key, mixed $value)`](#configset) | Set key/value                | self          |
| [`unset(string $key)`](#configunset)           | Remove value by key          | self          |
| [`add(string $key, mixed $value)`](#configadd) | Add a value to an array      | self          |
| [`remove(string $key)`](#configremove)         | Remove a value from an array | self          |
| [`values()`](#configvalues)                    | Get or set values            | array \| self |

```php title="Default setup"
<?php
use Symbiont\Config\{Config, ArrayConfig, JsonConfig};

$config = new Config([ /* options */ ]);
// same with ..
// $config = new ArrayConfig;
// $config = new JsonConfig;
$config->values([
    'test' => 'testing,
    'multiple' => [
        'values'
    ]
]);
```

### `get()`
```php
<?php
$config->get('test');
// returns `testing`
$config->get('doesntexist', 'default-value');
// returns `default-value`
$config->get('multiple');
// returns `['values']`
```

### `set()`
```php
<?php
$config->set('some-key', 'some-value');
// sets key `some-key` with value `some-value`
$config->set('multiple', 'testing');
// overwrites array value
```

### `unset()`
```php
<?php
$config->unset('some-key');
// removes key `some-key`
```

### `add()`
```php
<?php
$config->add('multiple', 'added');
// adds value `added` to key `multiple`
// results in `['values', 'added']`
```

### `remove()`
```php
<?php
$config->remove('multiple', 'values');
// removes value `values` from
// key `multiple` results in `['added']`
```

### `values()`
```php
<?php
$config->values();
// without arguments returns the current state of the config object is returned
$config->values([
    'new' => 'setting'
]);
// sets new state of the config object (overwrite)
```

## Xpath

### Xpath delimiter

The default `Config` supports xpath like behaviour to navigate through an array. The default separator is `->`, whereas 
the default can be changed using `ConfigSettings::$xpath_delimiter` globally or locally on the config object using 
`Config->xpath_delimiter`

```php
<?php
use Symbiont\Config\Config;
use Symbiont\Config\ConfigSettings;

// locally per config object
$config = new Config;
$config->xpath_delimiter = '.';

// globally for all config objects
ConfigSettings::$xpath_delimiter = '.';
```

### Using xpath with `get`/`set`/`unset`/`add` and `remove`

Get a value using xpath.

```php
<?php
$config->values([
    'some' => [
        'nested' => [
            'value' => 'as a string'
        ]           
    ]
]);
$config->get('some->nested->value');
// returns `as a string`
$config->get('this->does->not->exist', 'default-value');
// returns `default-value`

$config->set('some->nested->value', 'new value');
$config->set('some->nested', [
    'new-key' => 'with-value'
]);
// sets a new value to `some->nested->value`.
$config->unset('some->nested->new-key');
// removes key `new-key` from array `some->nested`
$config->add('some->nested', [
    'value' => 'as a string'
]);
// adds key with value `as a string` to array `nested`.
$config->remove('some->nested', 'value');
// removes key value from array `nested`
```

### Set values with xpath

```php
<?php
use Symbiont\Support\Config;

$config = new Config;
$config->values([

]);
```

## File based config Drivers

| Type                    | Driver                       |
|:------------------------|------------------------------|
| [PHP Array](#php-array) | Drivers\ArrayConfigDriver    |
| [JSON](#json)           | Drivers\JsonConfigDriver     |

### Loading a configuration from file

The path to any file is by default relative to the projects root folder. Example project structure:

```bash
 - configs/
   - project.json
 - src/
   - SomeClass.php
 - tests/
   - unit/
   - bootstrap.php
 - vendor/
 - .gitignore
```

```php
<?php
use Symbiont\Config\Config;

$config = Config::from('./configs/project.json');
// Creates a ArrayConfig object with `./configs/project.json` as its source.
```

### PHP Array Configuration

PHP Array driver uses the `.php` file extension returning an array.

```php
<?php
return [
    'some' => 'value'
];
```

### JSON Configuration

JSON driver uses the `.json` file extension and PHP's native json extension [compiled in PHP](https://www.php.net/manual/en/json.installation.php){:target="_blank"} since version `8.0.0`. 

```json
{
  "some": "value"
}
```

### Env configuration

Will be included in [milestone v1.4](https://gitlab.com/symbiont/config/-/milestones/5#tab-issues){:target="_blank"} as a separate package.

### YAML configuration

Will be included in [milestone v1.4](https://gitlab.com/symbiont/config/-/milestones/5#tab-issues){:target="_blank"} as a separate package.

### Custom file based driver

- @todo

## Database based config drivers

| Type                    | Driver                       |
|:------------------------|------------------------------|
| [Mysql](#mysql)         | Drivers\MysqlDatabaseDrivers |

### Mysql

The MySQL based driver is a proof of concept to separate the complete logic of file driver based and database driver 
based. It's quite pointless to store configurations in the database unless its relationally relevant, e.g. user profile 
configurations.

### Custom database based driver

- @todo

## Callbacks

Each driver incorporates a simple lightweight [Dispatcher](https://gitlab.com/symbiont/dispatcher){:target="_blank"} for callbacks. (See the [DispatchesEvent](https://gitlab.com/symbiont/dispatcher/-/blob/main/src/Concerns/DispatchesEvent.php){:target="_blank"} trait)

Using the `on()` method, the following events will be triggered.

| Event       |                                          |
|-------------|------------------------------------------|
| `loading`   | Before the values get loaded from source |
| `loaded`    | After values has been loaded from source |
| `saving`    | Before saving to source                  |
| `saved`     | After saving to source                   |
| `storing`   | Before creating a new source             |
| `stored`    | After the new source was created         |
| `unlinking` | Before removing the source               |
| `unlinked`  | After the source has been removed        |

Events called at construction time of a config object (e.g. `loading` or `loaded`) can be assigned via the `Config`'s constructor `$options` parameter (including all other events)

```php
<?php

$config = new Config([
    'callbacks' => [
        'loading' => function($event) {
            // before the file was loaded
        }   
    ]
])
```

## Force value types

Will be included in [milestone v1.3](https://gitlab.com/symbiont/config/-/milestones/4#tab-issues){:target="_blank"}

## A lot of work to do ..

- YAML support through [PECL](https://bd808.com/pecl-file_formats-yaml/){:target="_blank"} and fallback to [dallgoot/yaml](https://github.com/dallgoot/yaml){:target="_blank"}
- Support for ENV files through [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv){:target="_blank"}
- Versioning of config files
- Schema validation and equivalents like [vanilla/garden-schema](https://github.com/vanilla/garden-schema){:target="_blank"}, [nette/schema](https://github.com/nette/schema){:target="_blank"} or [lezhnev74/pasvl](https://github.com/lezhnev74/pasvl){:target="_blank"}