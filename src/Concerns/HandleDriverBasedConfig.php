<?php

namespace Symbiont\Config\Concerns;

use Symbiont\Config\Contracts\Driver\DrivesConfig;
use Symbiont\Config\Contracts\IsDriverBased;
use Symbiont\Support\ForwardCall\ForwardsCall;

trait HandleDriverBasedConfig {

    use ForwardsCall;

    protected DrivesConfig $driver;

    protected function initializeHandleDriverBasedConfig(DrivesConfig $driver, array $callbacks = []) {
        $this->setDriver($driver);
        $this->setCallbacksFromOption($callbacks);
    }

    protected function setCallbacksFromOption(array $callbacks) {
        foreach($callbacks as $event => $callback) {
            $this->on($event, $callback->bindTo($this));
        }
    }

    public function setDriver(DrivesConfig $driver): IsDriverBased {
        $this->driver = $driver;
        return $this;
    }

    public function getDriver(): DrivesConfig {
        return $this->driver;
    }

    public function hasDriver(): bool {
        return ! empty($this->driver);
    }

    public function forwardDriver(): array {
        return ['on', 'once', 'off', 'offAll'];
    }

}