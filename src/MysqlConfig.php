<?php

namespace Symbiont\Config;

use Symbiont\Config\Concerns\HandleDriverBasedConfig;
use Symbiont\Config\Contracts\IsDriverBased;
use Symbiont\Config\Drivers\MysqlDatabaseDriver;

class MysqlConfig extends Config implements IsDriverBased  {

    use HandleDriverBasedConfig;

    protected mixed $target;
    protected mixed $field;
    protected string $table;

    public function __construct(array $options, string $table) {
        $this->bootTraits([
            HandleDriverBasedConfig::class => [
                'driver' => new Drivers\MysqlDatabaseDriver(
                    $options['driver'] ?? []
                )
            ]
        ], [
            HandleDriverBasedConfig::class
        ]);

        parent::__construct($options);

        $this->table = $table;
    }

    public function row(mixed $target, $field = 'id') {
        return $this->driver->row(
            target: $target,
            field: $field,
            table: $this->table
        );
    }

    public function load(mixed $target, $field = 'id') {
        $this->target = $target;
        $this->field = $field;

        $this->values($this->row($target, $field));
        return $this;
    }

    public function save() {
        return $this->driver->save(
            values: $this->values,
            target: $this->target,
            field: $this->field,
            table: $this->table
        );
    }

}