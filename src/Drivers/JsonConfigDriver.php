<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Config\Concerns\Driver\HandleJson;
use Symbiont\Config\Contracts\Driver\UsesJson;
use Symbiont\Config\Exceptions\JsonDecodeErrorException;

class JsonConfigDriver extends FileDriver implements UsesJson {

    use HandleJson;

    protected function loadFile(string $file) {
        if(! is_array($decoded = $this->fromJson(file_get_contents($file)))) {
            throw new JsonDecodeErrorException($file, json_last_error_msg());
        }

        return $decoded;
    }

    protected function saveFile(string $file, array $values): bool {
        return (bool) file_put_contents($file, $this->toJson($values), self::$json_enc_flags);
    }

}