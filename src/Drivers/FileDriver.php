<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Config\Concerns\Driver\HandleFileBasedDrivers;
use Symbiont\Config\Contracts\Driver\DriverIsBasedOnFiles;

abstract class FileDriver extends ConfigDriver implements DriverIsBasedOnFiles {

    const OPTION_DO_NOT_LOAD_FILE = 1;
    const OPTION_CREATE_FILE = 2;
    const OPTION_ALLOW_WITHOUT_FILE = 4;

    use HandleFileBasedDrivers;

}