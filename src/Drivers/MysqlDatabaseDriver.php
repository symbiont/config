<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Config\Concerns\Driver\HandleJson;
use Symbiont\Config\Contracts\Driver\UsesJson;

class MysqlDatabaseDriver extends DatabaseDriver implements UsesJson {

    use HandleJson;

    protected function validateOptions(array $options): array {
        return array_merge([
            'host' => '127.0.0.1',
            'port' => 3306,
            'user' => null,
            'pass' => null,
            'name' => null
        ], $options);
    }

    public function connect(): DatabaseDriver {
        // how to connect to database, return connection
        $this->connection = new \mysqli(
            $this->options['host'], $this->options['user'], $this->options['pass'], $this->options['name'], $this->options['port']);
        return $this;
    }

    public function disconnect(): bool {
        // how to disconnect from database
        return $this->connection->close();
    }

    public function row($target, $field, $table): mixed {
        $stmt = $this->connection->prepare("SELECT {$this->field} FROM {$table} WHERE {$field} = ?");
        $stmt->bind_param('s', $target);
        if(! $stmt->execute()) {
            throw new \Exception('Figure out mysqli exceptions');
        }
        $result = ($stmt->get_result())->fetch_assoc();

        return $this->fromJson($result[$this->field]);
    }

    public function save(array $values, mixed $target, string $field, string $table): bool {
        $stmt = $this->connection->prepare("UPDATE {$table} SET {$this->field} = ? WHERE {$field} = ?");
        $values = $this->toJson($values);
        $stmt->bind_param('ss', $values, $target);
        if(! $stmt->execute()) {
            throw new \Exception('Figure out mysqli exceptions');
        }

        return true;
    }



}