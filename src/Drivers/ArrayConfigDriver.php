<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Config\Exceptions\ArrayDriverExpectsArrayException;

class ArrayConfigDriver extends FileDriver {

    public static function getFileExtension(): string {
        return 'php';
    }

    protected function loadFile(string $file) {
        $content = include $file;
        if(! is_array($content)) {
            throw new ArrayDriverExpectsArrayException($file, gettype($content));
        }
        return $content;
    }

    protected function saveFile(string $file, array $values): bool {
        return (bool) file_put_contents($file, "<?php \n\n return " . $this->toArrayString($values));
    }

    protected function toArrayString(array $values): string {
        $export = str_replace(
            ['array (', '),', ');'],
            ['[', '],', '];'],
            var_export($values, true) . ";"
        );

        return $export;
    }

}