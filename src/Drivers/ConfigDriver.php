<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Dispatcher\Contracts\Callbackable;
use Symbiont\Support\BootTrait\BootsTrait;
use Symbiont\Support\BootTrait\Contracts\BootsTraits;

use Symbiont\Config\Concerns\HandlesOptions;
use Symbiont\Config\Contracts\Optionable;
use Symbiont\Config\Contracts\Driver\DrivesConfig;

abstract class ConfigDriver implements DrivesConfig, Optionable, Callbackable, BootsTraits {

    use HandlesOptions,
        BootsTrait;

    public function __construct(array $options = []) {
        $this->bootTraits([
            HandlesOptions::class => [
                'options' => $options
            ]
        ]);
    }

}