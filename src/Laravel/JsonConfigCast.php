<?php

namespace Symbiont\Config\Laravel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\JsonConfig;

class JsonConfigCast implements CastsAttributes {

    public function get(Model $model, string $key, mixed $value, array $attributes): Configurable {
        return (new JsonConfig)->fromJson($value);
    }

    public function set(Model $model, string $key, mixed $config, array $attributes): string {
        return $config->toJson(flags: 0);
    }

}