<?php

namespace Symbiont\Config;

use Symbiont;
use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\BootsTrait;

class BaseConfig implements Contracts\Configurable, Contracts\Optionable, BootsTraits {

    use Concerns\HandlesOptions,
        Concerns\Array\HandleArrayPath,
        BootsTrait;

    protected array $values = [];

    const ADD_UNIQUE_VALUES = 1;
    const ADD_ONLY_ARRAYS = 2;
    const ADD_IF_NOT_EXISTS = 4;

    public function __construct(array $options = []) {
         $this->bootTraits([
             Symbiont\Config\Concerns\HandlesOptions::class => [
                 'options' => $options
             ]
         ]);
    }

    public function values(array $values = null): array | Contracts\Configurable {
        if($values === null) {
            return $this->values;
        }
        $this->values = $values;

        return $this;
    }

    public function get(string $key, $default = null) {
        return $this->values[$key] ?? $default;
    }

    public function set(string $key, mixed $value): Contracts\Configurable {
        $this->values[$key] = $value;
        return $this;
    }

    public function unset(string $key): Contracts\Configurable {
        if(array_key_exists($key, $this->values)) {
            unset($this->values[$key]);
        }

        return $this;
    }

    public function add(string $key, mixed $value, int $flags = 0): Contracts\Configurable {
        if(! ($current = $this->values[$key] ?? null)) {
            return $this->set($key, is_array($value) ? $value : [$value]);
        }

        if(! is_array($current) && $flags & self::ADD_ONLY_ARRAYS) {
            throw new Exceptions\AddOnlyAllowedOnArraysException(gettype($current));
        }

        $this->values[$key] = array_merge(
            $this->values[$key],
            is_array($value) ?
                $value :
                [$value]
        );

        if($flags & self::ADD_UNIQUE_VALUES) {
            $this->values[$key] = array_values(array_unique($this->values[$key]));
        }

        return $this;
    }

    public function remove(string $key, mixed $value): Contracts\Configurable {
        if(! array_key_exists($key, $this->values)) {
            return $this;
        }

        foreach(is_array($value) ? $value : [$value] as $value) {
            if (($location = array_search($value, $this->values[$key])) !== false) {
                unset($this->values[$key][$location]);
                $this->values[$key] = array_values($this->values[$key]);
            }
        }

        return $this;
    }

    public function hasKey(string $path, string $key = null) {
        if($key === null) {
            return array_key_exists($path, $this->values);
        }

        return array_key_exists($path, $this->values) && array_key_exists($key, $this->values[$path]);
    }

    public function hasValue(string $path, string $value) {
        if(array_key_exists($path, $this->values)) {
            return in_array($value, $this->values[$path], true);
        }

        return false;
    }

}