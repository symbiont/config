<?php

namespace Symbiont\Config\Contracts\Array;

interface ArrayPaths {

    public function keyIsPath(string|array $key): bool;
    public function extractKeyPath(string|array $key): array;
    public function getByPath(string|array $key, array $values): mixed;
    public function setByPath(string|array $key, array &$values, $value): mixed;
    public function unsetByPath(string|array $key, array &$values): mixed;
    public function removeByPath(string|array $key, array &$values, mixed $value): mixed;

}