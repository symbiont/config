<?php

namespace Symbiont\Config\Contracts\Driver;

interface ConnectsToDatabase {

    public function connect(): mixed;
    public function disconnect(): bool;

}