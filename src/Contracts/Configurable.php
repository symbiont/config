<?php

namespace Symbiont\Config\Contracts;

use Symbiont\Config\Contracts\Driver\DrivesConfig;

interface Configurable {

    public function values(array $values = null): array|Configurable;
    public function get(string $key, $default = null);
    public function set(string $key, mixed $value): Configurable;
    public function unset(string $key): Configurable;
    public function add(string $key, mixed $value, int $flags = 0): Configurable;
    public function remove(string $key, mixed $value): Configurable;

}