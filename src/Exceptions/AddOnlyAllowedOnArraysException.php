<?php

namespace Symbiont\Config\Exceptions;

class AddOnlyAllowedOnArraysException extends Exception {
    public function __construct(string $type) {
        parent::__construct(sprintf('Add only allowed on arrays, `%s` given', $type));
    }
}