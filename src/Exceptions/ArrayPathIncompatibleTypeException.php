<?php

namespace Symbiont\Config\Exceptions;

class ArrayPathIncompatibleTypeException extends Exception {
    public function __construct(string $type) {
        parent::__construct('Unable to add given value to type %s');
    }
}