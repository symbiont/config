<?php

namespace Symbiont\Config\Exceptions;

class MissingDriverException extends Exception {
    public function __construct() {
        parent::__construct('Config driver not set, use `setDriver(DrivesConfig $driver) to set a driver');
    }
}