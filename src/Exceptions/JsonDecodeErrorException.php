<?php

namespace Symbiont\Config\Exceptions;

class JsonDecodeErrorException extends Exception {
    public function __construct(string $file, string $error) {
        parent::__construct(sprintf('Unable to decode json file `%s`, error: `%s`', $file, $error));
    }
}