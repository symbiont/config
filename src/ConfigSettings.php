<?php

namespace Symbiont\Config;

final class ConfigSettings {

    public static array $drivers = [
        'json' => JsonConfig::class,
        'php' => ArrayConfig::class
    ];

    const XPATH_DELIMITER = '->';
    const XPATH_THROW_EXCEPTION = true;

    public static string $xpath_delimiter = ConfigSettings::XPATH_DELIMITER;
    public static bool $xpath_throw_exception = ConfigSettings::XPATH_THROW_EXCEPTION;

}